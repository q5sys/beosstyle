# BeOSStyle

BeOS-inspired widget style for Qt5

## Disclaimer

This code is incomplete and unsupported. It is provided for nostalgic reasons
only.

## Usage

The BeOSStyle class cannot be used by itself. Instead, it needs to be
incorporated into a library that provides a Qt style plugin. The glue code to do
that is trivial, but is not provided here due to potential licence
incompatibility.

One easy option for using this style is to follow these steps:
1. Obtain the other Qt style plugins from https://github.com/qt/qtstyleplugins
2. Add a beos directory under src/plugins/styles
3. Add a class derived from QStylePlugin, a project file and a json file,
   mimicking the code and structure of the other styles.

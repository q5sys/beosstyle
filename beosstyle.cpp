/*
 * Copyright (C) 2019 Elad Lahav. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "beosstyle.h"
#include <QStyleFactory>
#include <QPainter>
#include <QFont>
#include <QStyleOption>
#include <QDebug>
#include <QMenu>

/**
 * Class constructor.
 */
BeOSStyle::BeOSStyle() : QCommonStyle()
{
}

/**
 * Choose a colour palette for the style.
 * @param[out]  pal     The paletter to use
 */
void
BeOSStyle::polish(QPalette &pal)
{
    pal = QPalette(QColor(216, 216, 216));
}

/**
 * Apply last-minute changes to any widgets prior to display.
 * @param   widget  The widget being displayed
 */
void
BeOSStyle::polish(QWidget *widget)
{
    if (qobject_cast<QMenu *>(widget)) {
        widget->setAttribute(Qt::WA_Hover);
    }
}

/**
 * Draw text.
 * The function is overridden from the default in order to choose a different
 * font.
 * @param   painter     Device used for drawing
 * @param   rectangle   Bounding rectabgle
 * @param   alignment   Text alignment
 * @param   palettte    Colour scheme
 * @param   enabled     Whether the text belongs to an enabled widget
 * @param   text        The text to draw
 * @param   role        Ignored
 */
void
BeOSStyle::drawItemText(
    QPainter            *painter,
    const QRect         &rectangle,
    int                 alignment,
    const QPalette      &palette,
    bool                enabled,
    const QString       &text,
    QPalette::ColorRole textRole) const
{
    Q_UNUSED(palette);
    Q_UNUSED(enabled);
    Q_UNUSED(textRole);

    QFont font("Helvetica", 10);
    painter->setFont(font);
    painter->drawText(rectangle, alignment, text);
}

/**
 * Override the default values for various element sizes.
 * @param   metric  A sized element
 * @param   option  Additional options, such as widget state
 * @param   widget  The widget being drawn
 * @return  The size, in pixels, of the requested element
 */
int
BeOSStyle::pixelMetric(
    PixelMetric         metric,
    const QStyleOption  *option,
    const QWidget       *widget) const
{
    switch (metric) {
    case PM_DefaultFrameWidth:
        return 1;
    case PM_ScrollBarExtent:
        return 15;
    default:
        return QCommonStyle::pixelMetric(metric, option, widget);
    }
}

/**
 * Draw a basic element.
 * @param   element     The type of element to draw
 * @param   option      Additional options, such as widget state
 * @param   painter     Device used for drawing
 * @param   widget      The widget being drawn
 */
void
BeOSStyle::drawPrimitive(
    PrimitiveElement    element,
    const QStyleOption  *option,
    QPainter            *painter,
    const QWidget       *widget) const
{
    int x;
    int y;
    int width;
    int height;

    // Save the current state so it can be restored after the modifications
    // below.
    painter->save();

    // Get the bounding rectangle of the element being drawn.
    option->rect.getRect(&x, &y, &width, &height);

    switch (element) {
    case PE_IndicatorCheckBox:
        {
            QImage checkBox(option->state & State_On ?
                            ":/images/beos_checked_box.xpm" :
                            ":/images/beos_unchecked_box.xpm");
            painter->drawImage(option->rect.topLeft(), checkBox);
        }
        break;
    case PE_IndicatorRadioButton:
        {
            QImage checkBox(option->state & State_On ?
                            ":/images/beos_checked_radio.xpm" :
                            ":/images/beos_unchecked_radio.xpm");
            painter->drawImage(option->rect.topLeft(), checkBox);
        }
        break;
    case PE_PanelButtonCommand:
        {
            painter->setPen(QColor(96, 96, 96));
            painter->drawLine(x + 1, y, x + width - 2, y);
            painter->drawLine(x, y + 1, x, y + height - 2);
            painter->drawLine(x + width - 1, y + 1, x + width - 1, y + height - 2);
            painter->drawLine(x + 1, y + height - 1, x + width - 2, y + height - 1);
            painter->setPen(QColor(232, 232, 232));
            painter->drawLine(x + 1, y + 1, x + width - 2, y + 1);
            painter->drawLine(x + 1, y + 2, x + 1, y + height - 2);
            painter->setPen(QColor(152, 152, 152));
            painter->drawLine(x + width - 2, y + 2, x + width - 2, y + height - 2);
            painter->drawLine(x + 2, y + height - 2, x + width - 2, y + height - 2);
            painter->setPen(QColor(255, 255, 255));
            painter->drawLine(x + 2, y + 2, x + width - 3, y + 2);
            painter->drawLine(x + 2, y + 3, x + width - 4, y + 3);
            painter->drawLine(x + 2, y + 4, x + 2, y + height - 3);
            painter->drawLine(x + 3, y + 4, x + 3, y + height - 4);
            painter->setPen(QColor(216, 216, 216));
            painter->drawLine(x + 3, y + height - 3 , x + width - 3, y + height - 3);
            painter->drawLine(x + width - 3, y + 3, x + width - 3, y + height - 3);
        }
        break;
    case PE_IndicatorToolBarHandle:
        // BeOS doesn't seem to have such a handle.
        break;
    default:
        QCommonStyle::drawPrimitive(element, option, painter, widget);
    }

    // Restore painter state.
    painter->restore();
}

/**
 * Draw a control element.
 * @param   element     The type of element to draw
 * @param   option      Additional options, such as widget state
 * @param   painter     Device used for drawing
 * @param   widget      The widget being drawn
 */
void
BeOSStyle::drawControl(
    ControlElement      element,
    const QStyleOption  *option,
    QPainter            *painter,
    const QWidget       *widget) const
{
    int x;
    int y;
    int width;
    int height;

    // Save the current state so it can be restored after the modifications
    // below.
    painter->save();

    // Get the bounding rectangle of the element being drawn.
    option->rect.getRect(&x, &y, &width, &height);

    switch (element) {
    case CE_ScrollBarSubLine:
        {
            painter->setPen(QColor(152, 152, 152));
            painter->setBrush(QColor(152, 152, 152));
            painter->drawRect(option->rect);
            QImage arrow(option->state & State_Horizontal ?
                         ":/images/beos_left_arrow.xpm" :
                         ":/images/beos_up_arrow.xpm");
            if (option->state & State_Sunken) {
                x++;
                y++;
            }
            if (option->state & State_Horizontal) {
                y++;
            } else {
                x++;
            }
            painter->drawImage(x, y, arrow);
        }
        break;
    case CE_ScrollBarAddLine:
        {
            painter->setPen(QColor(152, 152, 152));
            painter->setBrush(QColor(152, 152, 152));
            painter->drawRect(option->rect);
            QImage arrow(option->state & State_Horizontal ?
                         ":/images/beos_right_arrow.xpm" :
                         ":/images/beos_down_arrow.xpm");
            if (option->state & State_Sunken) {
                x++;
                y++;
            }
            if (option->state & State_Horizontal) {
                y++;
            } else {
                x++;
            }
            painter->drawImage(x, y, arrow);
        }
        break;
    case CE_ScrollBarSubPage:
    case CE_ScrollBarAddPage:
        {
            painter->setPen(QColor(152, 152, 152));
            if (option->state & State_Horizontal) {
                painter->drawLine(x, y, x + width - 1, y);
                painter->drawLine(x, y + height - 1, x + width - 1, y + height - 1);
            } else {
                painter->drawLine(x, y, x, y + height - 1);
                painter->drawLine(x + width - 1, y, x + width - 1, y + height - 1);
            }
            painter->setPen(QColor(184, 184, 184));
            if (option->state & State_Horizontal) {
                painter->drawLine(x, y + 1, x + width - 1, y + 1);
                painter->drawLine(x, y + 2, x + width - 1, y + 2);
            } else {
                painter->drawLine(x + 1, y, x + 1, y + height - 1);
                painter->drawLine(x + 2, y, x + 2, y + height - 1);
            }
            painter->setPen(QColor(216, 216, 216));
            if (option->state & State_Horizontal) {
                painter->drawLine(x, y + height - 2, x + width - 1, y + height - 2);
            } else {
                painter->drawLine(x + width - 2, y, x + width - 2, y + height - 1);
            }
            painter->setPen(QColor(200, 200, 200));
            painter->setBrush(QColor(200, 200, 200));
            if (option->state & State_Horizontal) {
                painter->drawRect(x, y + 3, width - 1, height - 3);
            } else {
                painter->drawRect(x + 3, y, width - 3, height - 1);
            }
        }
        break;
    case CE_ScrollBarSlider:
        {
            painter->setPen(QColor(152, 152, 152));
            if (option->state & State_Horizontal) {
                painter->drawLine(x, y, x + width - 1, y);
                painter->drawLine(x, y + height - 1, x + width - 1, y + height - 1);
            } else {
                painter->drawLine(x, y, x, y + height - 1);
                painter->drawLine(x + width - 1, y, x + width - 1, y + height - 1);
            }
            painter->setPen(QColor(96, 96, 96));
            if (option->state & State_Horizontal) {
                painter->drawLine(x + width - 1, y + 1, x + width - 1, y + height - 2);
            } else {
                painter->drawLine(x + 1, y + height - 1, x + width - 2, y + height - 1);
            }
            painter->setPen(QColor(255, 255, 255));
            if (option->state & State_Horizontal) {
                painter->drawLine(x, y + 1, x + width - 3, y + 1);
                painter->drawLine(x, y + 2, x, y + height - 3);
            } else {
                painter->drawLine(x + 1, y, x + 1, y + height - 3);
                painter->drawLine(x + 2, y, x + width - 3, y);
            }
            painter->setPen(QColor(184, 184, 184));
            if (option->state & State_Horizontal) {
                painter->drawLine(x, y + height - 2, x + width - 2, y + height - 2);
                painter->drawLine(x + width - 2, y + 1, x + width - 2, y + height - 2);
            } else {
                painter->drawLine(x + width - 2, y, x + width - 2, y + height - 2);
                painter->drawLine(x + 1, y + height - 2, x + height - 2, y + height - 2);
            }
            painter->setPen(QColor(216, 216, 216));
            painter->setBrush(QColor(216, 216, 216));
            painter->drawRect(x + 1, y + 1, width - 3, height - 3);

            // Draw 1 or 3 squares on the slider.
            int nsquares;
            int dim = option->state & State_Horizontal ? width: height;
            if (dim > 20) {
                nsquares = 3;
                if (option->state & State_Horizontal) {
                    x += width / 2 - 8;
                    y += height / 2 - 1;
                } else {
                    x += width / 2 - 1;
                    y += height / 2 - 8;
                }
            } else {
                nsquares = 1;
                x += width / 2 - 2;
                y += height / 2 - 2;
            }

            for (int i = 0; i < nsquares; i++) {
                painter->setPen(QColor(255, 255, 255));
                painter->drawLine(x, y, x + 2, y);
                painter->drawLine(x, y + 1, x, y + 2);
                painter->setPen(QColor(184, 184, 184));
                painter->drawLine(x, y + 3, x + 3, y + 3);
                painter->drawLine(x + 3, y, x + 3, y + 2);
                if (option->state & State_Horizontal) {
                    x += 6;
                } else {
                    y += 6;
                }
            }
        }
        break;

    case CE_MenuItem:
        {
            const QStyleOptionMenuItem *menuOption
                    = qstyleoption_cast<const QStyleOptionMenuItem *>(option);

            if (option->state & State_MouseOver) {
                qDebug() << "Selected";
                painter->setBrush(QColor(128, 128, 128));
                painter->drawRect(option->rect);
            }

            // Draw text.
            QRect textRect = option->rect;
            textRect.adjust(16, 0, 0, 0);
            drawItemText(painter, textRect, Qt::AlignVCenter | Qt::TextShowMnemonic,
                         option->palette,
                         true, menuOption->text);

            // Draw check mark next to a checked item.
            if (menuOption->checked) {
                QImage check(":/images/beos_menu_check.xpm");
                painter->drawImage(x + 4, y + 4, check);
            }
        }
        break;
    default:
        QCommonStyle::drawControl(element, option, painter, widget);
    }
    painter->restore();
}

/**
 * Draw a multi-element control.
 * @param   element     The type of control to draw
 * @param   option      Additional options, such as widget state
 * @param   painter     Device used for drawing
 * @param   widget      The widget being drawn
 */
void
BeOSStyle::drawComplexControl(
    ComplexControl              element,
    const QStyleOptionComplex   *option,
    QPainter                    *painter,
    const QWidget               *widget) const
{
    int x;
    int y;
    int width;
    int height;

    // Save the current state so it can be restored after the modifications
    // below.
    painter->save();

    // Get the bounding rectangle of the control being drawn.
    option->rect.getRect(&x, &y, &width, &height);

    switch (element) {
    case CC_ComboBox:
        {
            painter->setPen(QColor(152, 152, 152));
            painter->drawLine(x, y, x + width - 3, y);
            painter->drawLine(x, y + 1, x, y + height - 3);
            painter->drawLine(x + 2, y + height - 1, x + width - 1, y + height - 1);
            painter->drawLine(x + width - 1, y + 2, x + width - 1, y + height - 2);
            painter->setPen(QColor(96, 96, 96));
            painter->drawLine(x, y + height - 2, x + width - 2, y + height - 2);
            painter->drawLine(x + width - 2, y, x + width - 2, y + height - 3);
            painter->setPen(QColor(216, 216, 216));
            painter->drawLine(x + 1, y + 1, x + width - 4, y + 1);
            painter->drawLine(x + 1, y + 2, x + 1, y + height - 3);
            painter->setPen(QColor(184, 184, 184));
            painter->drawLine(x + 2, y + height - 3, x + width - 3, y + height - 3);
            painter->drawLine(x + width - 3, y + 1, x + width - 3, y + height - 3);

            // Draw the arrow.
            painter->setPen(QColor(96, 96, 96));
            painter->drawLine(x + width - 10, y + height - 8, x + width - 6, y + height - 8);
            painter->drawLine(x + width - 9, y + height - 7, x + width - 7, y + height - 7);
            painter->drawLine(x + width - 8, y + height - 6, x + width - 8, y + height - 6);
        }
        break;
    default:
        QCommonStyle::drawComplexControl(element, option, painter, widget);
    }
    painter->restore();
}
